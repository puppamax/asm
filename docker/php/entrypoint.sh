#!/bin/bash

cd /var/www

# printenv > .env

# source .env

if [[ -z "${APP_KEY}" ]]; then
    echo "Generating encryption key"
    php artisan key:generate
fi

if [[ -z "${JWT_SECRET}" ]]; then
    echo "Generating JWT secret"
    php artisan jwt:secret -f
fi

echo "Restart artisan"
php artisan queue:restart

echo "Staring php-fpm"
cd /var/www/public && php-fpm